package org.taudev.demos.tmbs.model;

public enum SeatState {
    FREE, SELECTED, BOOKED, SOLD, RESERVED
}

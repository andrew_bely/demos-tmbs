package org.taudev.demos.tmbs.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "SHOWS")
public class Show {

	@Id
	@Column(name = "SHOW_ID")
	private Long id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "show_date")
	private Date date;

	@ManyToOne
	@JoinColumn(name = "MOVIE_ID")
	private Movie movie;

	@ManyToOne
	@JoinColumn(name = "HALL_ID")
	private Hall hall;

	public Long getId() {
		return id;
	}

	private void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	public Hall getHall() {
		return hall;
	}

	public void setHall(Hall hall) {
		this.hall = hall;
	}

}

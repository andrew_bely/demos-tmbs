package org.taudev.demos.tmbs.model;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "USERS")
public class User implements UserDetails {

	@Id
	@Column(name = "USER_ID")
	private Long id;

	@Column(name = "LOGIN")
	private String username;

	@Column(name = "PASSWD")
	private String password;

	private String email;

	@Temporal(TemporalType.TIMESTAMP)
	private Date registered;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "passwd_set")
	private Date passwordUpdated;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_login")
	private Date lastLogin;

	public Long getId() {
		return id;
	}

	private void setId(Long id) {
		this.id = id;
	}

	@Override
	public String getUsername() {
		return username;
	}

	public void setUsername(String login) {
		this.username = login;
	}

	@Override
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO add account state; check account state and password age here
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO add account state; check account state and password age here
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO add account state; check account state and password age here
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO add account state; check account state and password age here
		return true;
	}

}

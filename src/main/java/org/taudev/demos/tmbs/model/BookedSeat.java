package org.taudev.demos.tmbs.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "BOOKED_SEATS")
@IdClass(BookedSeat.BookedSeatCompositePrimaryKey.class)
public class BookedSeat {

	@Column(name = "BOOKING_ID")
	private Long bookingId;

	@Id
	@Column(name = "SHOW_ID")
	private Long showId;

	@Id
	@Column(name = "ROW_NUM", nullable = false)
	private Integer row;

	@Id
	@Column(name = "SEAT", nullable = false)
	private Integer seat;

	@Enumerated(EnumType.ORDINAL)
	private SeatState state;

	public Long getBookingId() {
		return bookingId;
	}

	public void setBookingId(Long bookingId) {
		this.bookingId = bookingId;
	}

	public Long getShowId() {
		return showId;
	}

	public void setShowId(Long showId) {
		this.showId = showId;
	}

	public Integer getRow() {
		return row;
	}

	public void setRow(Integer row) {
		this.row = row;
	}

	public Integer getSeat() {
		return seat;
	}

	public void setSeat(Integer place) {
		this.seat = place;
	}

	public SeatState getState() {
		return state;
	}

	public void setState(SeatState state) {
		this.state = state;
	}

	public static class BookedSeatCompositePrimaryKey implements Serializable {

		private static final long serialVersionUID = -6247399855285038900L;

		private Long showId;
		private Integer row;
		private Integer seat;

		public Long getShowId() {
			return showId;
		}

		public void setShowId(Long showId) {
			this.showId = showId;
		}

		public Integer getRow() {
			return row;
		}

		public void setRow(Integer row) {
			this.row = row;
		}

		public Integer getSeat() {
			return seat;
		}

		public void setSeat(Integer seat) {
			this.seat = seat;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((row == null) ? 0 : row.hashCode());
			result = prime * result + ((seat == null) ? 0 : seat.hashCode());
			result = prime * result + ((showId == null) ? 0 : showId.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			BookedSeatCompositePrimaryKey other = (BookedSeatCompositePrimaryKey) obj;
			if (row == null) {
				if (other.row != null)
					return false;
			}
			else if (!row.equals(other.row))
				return false;
			if (seat == null) {
				if (other.seat != null)
					return false;
			}
			else if (!seat.equals(other.seat))
				return false;
			if (showId == null) {
				if (other.showId != null)
					return false;
			}
			else if (!showId.equals(other.showId))
				return false;
			return true;
		}

	}
}

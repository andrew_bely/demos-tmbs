package org.taudev.demos.tmbs.model;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class ShowSeatInfo {

    // @Column(name = "SHOW_ID")
    // private Long showId;

    @Column(name = "BOOKING_ID")
    private Long bookingId;

    @Column(name = "ROW")
    private Integer row;

    @Column(name = "SEAT")
    private Integer seat;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "SEAT_STATE")
    private SeatState state;

    @Column(name = "OWNER_ID")
    private Integer ownerId;

    /*
     * public Long getShowId() { return showId; }
     * 
     * public void setShowId(Long showId) { this.showId = showId; }
     */

    public Long getBookingId() {
        return bookingId;
    }

    public void setBookingId(Long bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public Integer getSeat() {
        return seat;
    }

    public void setSeat(Integer seat) {
        this.seat = seat;
    }

    public SeatState getState() {
        return state;
    }

    public void setState(SeatState state) {
        this.state = state;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

}

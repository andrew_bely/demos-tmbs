package org.taudev.demos.tmbs.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "HALLS")
public class Hall {

    @Id
    @Column(name = "HALL_ID")
    private Integer id;

    @Column(name="HALL_NAME")
    private String name;

    @OneToMany
    @JoinColumn(name = "HALL_ID")
    private List<HallRows> rows;

    public Integer getId() {
        return id;
    }

    private void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<HallRows> getRows() {
        return rows;
    }

    public void setRows(List<HallRows> rows) {
        this.rows = rows;
    }

}

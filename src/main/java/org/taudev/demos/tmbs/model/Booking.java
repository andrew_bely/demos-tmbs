package org.taudev.demos.tmbs.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "BOOKINGS")
public class Booking {

	@Id
	@Column(name = "BOOKING_ID")
	private Long id;

	@ManyToOne
	@JoinColumn(name = "OWNER_ID")
	private User owner;

	@OneToMany(targetEntity = BookedSeat.class)
	@JoinColumn(name = "BOOKING_ID")
	private List<BookedSeat> seats;

}

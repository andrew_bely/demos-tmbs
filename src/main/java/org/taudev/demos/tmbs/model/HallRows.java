package org.taudev.demos.tmbs.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "HALL_SEATS")
@IdClass(HallRows.HallRowsCompositeKey.class)
public class HallRows {

    @Id
    @Column(name = "HALL_ID")
    private Integer hallId;

    @Id
    @Column(name = "START_ROW")
    private Integer startRow;

    @Id
    @Column(name = "END_ROW")
    private Integer endRow;

    @Column(name="NUM_SEATS")
    private Integer length;

    public Integer getHallId() {
        return hallId;
    }

    public void setHallId(Integer hallId) {
        this.hallId = hallId;
    }

    public Integer getStartRow() {
        return startRow;
    }

    public void setStartRow(Integer startRow) {
        this.startRow = startRow;
    }

    public Integer getEndRow() {
        return endRow;
    }

    public void setEndRow(Integer endRow) {
        this.endRow = endRow;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public static class HallRowsCompositeKey implements Serializable {
        private static final long serialVersionUID = 348976081525634637L;

        private Integer hallId;
        private Integer startRow;
        private Integer endRow;

        public Integer getHallId() {
            return hallId;
        }

        public void setHallId(Integer hallId) {
            this.hallId = hallId;
        }

        public Integer getStartRow() {
            return startRow;
        }

        public void setStartRow(Integer startRow) {
            this.startRow = startRow;
        }

        public Integer getEndRow() {
            return endRow;
        }

        public void setEndRow(Integer endRow) {
            this.endRow = endRow;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((endRow == null) ? 0 : endRow.hashCode());
            result = prime * result + ((hallId == null) ? 0 : hallId.hashCode());
            result = prime * result + ((startRow == null) ? 0 : startRow.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            HallRowsCompositeKey other = (HallRowsCompositeKey) obj;
            if (endRow == null) {
                if (other.endRow != null)
                    return false;
            } else if (!endRow.equals(other.endRow))
                return false;
            if (hallId == null) {
                if (other.hallId != null)
                    return false;
            } else if (!hallId.equals(other.hallId))
                return false;
            if (startRow == null) {
                if (other.startRow != null)
                    return false;
            } else if (!startRow.equals(other.startRow))
                return false;
            return true;
        }
    }
}

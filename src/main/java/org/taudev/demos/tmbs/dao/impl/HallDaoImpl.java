package org.taudev.demos.tmbs.dao.impl;

import org.taudev.demos.tmbs.model.Hall;
import org.taudev.persistence.dao.GenericDao;

public interface HallDaoImpl extends GenericDao<Hall, Integer> {

}

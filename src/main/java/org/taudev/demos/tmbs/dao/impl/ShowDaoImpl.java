package org.taudev.demos.tmbs.dao.impl;

import org.springframework.stereotype.Component;
import org.taudev.demos.tmbs.model.Show;
import org.taudev.persistence.dao.GenericCrudJpaDao;

@Component
public class ShowDaoImpl extends GenericCrudJpaDao<Show, Long> {

}

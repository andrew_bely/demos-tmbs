package org.taudev.demos.tmbs.dao.impl;

import javax.persistence.NoResultException;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.taudev.demos.tmbs.model.User;
import org.taudev.demos.tmbs.model.User_;
import org.taudev.persistence.dao.GenericCrudJpaDao;

public class UserDao extends GenericCrudJpaDao<User, Integer> implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = null;
		try {
			user = findOneByAttribute(User_.username, username);
		} catch (NoResultException nre) {
			throw new UsernameNotFoundException("UserDao: no such user \"" + username + "\"", nre);
		}
		return user;
	}
}

package org.taudev.demos.tmbs.dao;

import java.util.List;

import org.taudev.demos.tmbs.model.Booking;
import org.taudev.demos.tmbs.model.ShowSeatInfo;
import org.taudev.persistence.dao.GenericDao;

public interface BookingDao extends GenericDao<Booking, Long> {

    List<Booking> getBookingForShow(long showId);

    List<ShowSeatInfo> getBookedPlacesForShow(long showId);
}

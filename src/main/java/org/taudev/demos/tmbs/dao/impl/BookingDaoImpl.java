package org.taudev.demos.tmbs.dao.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.ListJoin;
import javax.persistence.criteria.Root;

import org.taudev.demos.tmbs.dao.BookingDao;
import org.taudev.demos.tmbs.model.BookedSeat;
import org.taudev.demos.tmbs.model.BookedSeat_;
import org.taudev.demos.tmbs.model.Booking;
import org.taudev.demos.tmbs.model.Booking_;
import org.taudev.demos.tmbs.model.ShowSeatInfo;
import org.taudev.demos.tmbs.model.User;
import org.taudev.demos.tmbs.model.User_;
import org.taudev.persistence.dao.GenericCrudJpaDao;

public class BookingDaoImpl extends GenericCrudJpaDao<Booking, Long> implements BookingDao {

	@Override
	public List<Booking> getBookingForShow(long showId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ShowSeatInfo> getBookedPlacesForShow(long showId) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<ShowSeatInfo> query = cb.createQuery(ShowSeatInfo.class);
		Root<Booking> queryRoot = query.from(Booking.class);
		ListJoin<Booking, BookedSeat> bookedSeatJoin = queryRoot.join(Booking_.seats);
		Join<Booking, User> userJoin = queryRoot.join(Booking_.owner);
		query.multiselect(queryRoot.get(Booking_.id).alias("BOOKING_ID"),
				userJoin.get(User_.id).alias("OWNER_ID"),
				bookedSeatJoin.get(BookedSeat_.state).alias("SEAT_STATE"), bookedSeatJoin.get(BookedSeat_.row),
				bookedSeatJoin.get(BookedSeat_.seat));
		query.where(cb.equal(bookedSeatJoin.get(BookedSeat_.showId), Long.valueOf(showId)));

		return em.createQuery(query).getResultList();
	}
}

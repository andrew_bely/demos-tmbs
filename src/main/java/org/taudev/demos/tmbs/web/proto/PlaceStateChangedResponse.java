package org.taudev.demos.tmbs.web.proto;

import org.taudev.demos.tmbs.model.SeatState;

public class PlaceStateChangedResponse {
    private boolean succeded;
    private SeatState state;
    private boolean ownedByYou;

    public PlaceStateChangedResponse(boolean succeded, SeatState state, boolean ownedByYou) {
        super();
        this.succeded = succeded;
        this.state = state;
        this.ownedByYou = ownedByYou;
    }

    public boolean isSucceded() {
        return succeded;
    }

    public SeatState getState() {
        return state;
    }

    public boolean isOwnedByYou() {
        return ownedByYou;
    }

}

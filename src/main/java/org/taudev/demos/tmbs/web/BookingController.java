package org.taudev.demos.tmbs.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.taudev.demos.tmbs.model.SeatState;
import org.taudev.demos.tmbs.model.ShowSeatInfo;
import org.taudev.demos.tmbs.model.User;
import org.taudev.demos.tmbs.service.BookingService;
import org.taudev.demos.tmbs.web.proto.PlaceStateChangedResponse;

@Controller
public class BookingController {

	@Autowired
	private BookingService bookingService;

	public BookingController(BookingService bookingService) {
		super();
		this.bookingService = bookingService;
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public PlaceStateChangedResponse onSelectPlace(@RequestParam long showId, @RequestParam int row,
			@RequestParam int seat) {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		boolean succeded = bookingService.selectSeat(showId, row, seat, user);
		PlaceStateChangedResponse response;
		if (succeded) {
			response = new PlaceStateChangedResponse(succeded, SeatState.SELECTED, true);
		}
		else {
			ShowSeatInfo seatInfo = bookingService.getSeatInfo(showId, row, seat);
			response = new PlaceStateChangedResponse(succeded, seatInfo.getState(), user.getId().equals(
					seatInfo.getOwnerId()));
		}

		return response;
	}

	public void getHallMap(@RequestParam long showId) {

	}

	public void getSeatsStatusUpdate(@RequestParam long showId) {

	}

	public void completeBooking(long showId) {

	}

}

package org.taudev.demos.tmbs.misc;

import java.util.List;
import java.util.concurrent.atomic.AtomicReferenceArray;

import org.taudev.demos.tmbs.model.SeatState;

public class ShowHallMap {
    private List<AtomicReferenceArray<SeatState>> seats;
    private int[][] owners;

    public ShowHallMap(List<AtomicReferenceArray<SeatState>> seats) {
        super();
        this.seats = seats;
    }

    private List<AtomicReferenceArray<SeatState>> getSeats() {
        return seats;
    }

    private int[][] getOwners() {
        return owners;
    }

    private void setOwners(int[][] owners) {
        this.owners = owners;
    }

}

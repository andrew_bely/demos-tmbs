package org.taudev.demos.tmbs.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReferenceArray;

import javax.transaction.Transactional;

import org.taudev.demos.tmbs.dao.BookingDao;
import org.taudev.demos.tmbs.misc.ShowHallMap;
import org.taudev.demos.tmbs.model.SeatState;
import org.taudev.demos.tmbs.model.Show;
import org.taudev.demos.tmbs.model.ShowSeatInfo;
import org.taudev.demos.tmbs.model.User;
import org.taudev.persistence.dao.GenericDao;

public class BookingServiceImpl implements BookingService {

	private GenericDao<User, Long> userDao;
	private GenericDao<Show, Long> showDao;
	private BookingDao bookingDao;

	private Map<Long, List<AtomicReferenceArray<SeatState>>> showSeatsState;
	private Map<Long, int[][]> showSeatsOwners;

	private static final List<AtomicReferenceArray<SeatState>> dummyHall = new ArrayList<AtomicReferenceArray<SeatState>>(
			1);

	public BookingServiceImpl(BookingDao bookingDao, GenericDao<User, Long> userDao,
			GenericDao<Show, Long> showDao) {
		super();
		this.userDao = userDao;
		this.showDao = showDao;
		this.bookingDao = bookingDao;
		showSeatsState = new ConcurrentHashMap<Long, List<AtomicReferenceArray<SeatState>>>();
	}

	@Override
	public Show getShow(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional
	@Override
	public ShowSeatInfo getSeatInfo(long showId, int row, int seat) {

		return null;
	}

	@Override
	public boolean selectSeat(long showId, int row, int seat, User user) {
		return false;
	}

	public ShowHallMap buildHallMap(long showId) {

		return null;
	}
}

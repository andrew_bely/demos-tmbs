package org.taudev.demos.tmbs.service;

import org.taudev.demos.tmbs.model.Show;
import org.taudev.demos.tmbs.model.ShowSeatInfo;
import org.taudev.demos.tmbs.model.User;

public interface BookingService {

	Show getShow(long id);

	ShowSeatInfo getSeatInfo(long showId, int row, int place);

	boolean selectSeat(long showId, int row, int place, User user);
}

package org.taudev.demos.sorting.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.taudev.demos.sorting.model.Employee;
import org.taudev.persistence.dao.GenericDao;

@Controller
public class SortingController {

    @Autowired
    GenericDao<Employee, Integer> employeeDao;

    @RequestMapping(value = "/demo/sorting.html", method = RequestMethod.GET)
    public ModelAndView getSortingPage() {
        List<Employee> allEmployees = employeeDao.loadAll();
        ModelAndView modelAndView = new ModelAndView("sorting");
        modelAndView.addObject("employees", allEmployees);
        return new ModelAndView("sorting");
    }

    @RequestMapping(value = "/demo/sorting.html", method = RequestMethod.POST)
    public ModelAndView getSortedPage(@RequestParam String order1, @RequestParam String order2, @RequestParam String order3) {
        ModelAndView modelAndView = new ModelAndView("sorting");
        List<Employee> allEmployees = employeeDao.loadAll();
        
        modelAndView.addObject("employees", allEmployees);
        return modelAndView;
    }
}

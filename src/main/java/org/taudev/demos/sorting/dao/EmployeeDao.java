package org.taudev.demos.sorting.dao;

import org.taudev.demos.sorting.model.Employee;
import org.taudev.persistence.dao.GenericCrudJpaDao;

public class EmployeeDao extends GenericCrudJpaDao<Employee, Integer> {

}

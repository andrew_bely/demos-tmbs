package org.taudev.demos.sorting.compare;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.taudev.demos.sorting.model.Employee;

public class EmployeeComparatorBuilder {

	private static final Map<String, Comparator<Employee>> knownComparators;

	static {
		Map<String, Comparator<Employee>> comparators = new HashMap<String, Comparator<Employee>>();
		comparators.put("name", new NameEmployeeComparator());
		comparators.put("age", new AgeEmployeeComparator());
		comparators.put("salary", new SalaryEmployeeComparator());
		knownComparators = Collections.unmodifiableMap(comparators);
	}

	public static Comparator<Employee> buildEmployeeComparator(List<ComparisonParam> params) {
		Comparator<Employee> comparator = null;
		if (params.size() == 1) {
			comparator = knownComparators.get(params.get(0).getName());
			if (comparator != null) {
				if (params.get(0).isReverse()) {
					comparator = new ReversingOrderComparator<Employee>(comparator);
				}
			}
		}
		else {
			List<Comparator<Employee>> comparators = new ArrayList<Comparator<Employee>>(params.size());
			for (ComparisonParam param : params) {
				Comparator<Employee> cmp = knownComparators.get(param.getName());
				if (cmp != null) {
					comparators.add(param.isReverse() ? new ReversingOrderComparator<Employee>(cmp) : cmp);
				}
			}
			if (!comparators.isEmpty()) {
				comparator = new ChainedComparator<Employee>(comparators);
			}
		}
		return comparator;
	}
}

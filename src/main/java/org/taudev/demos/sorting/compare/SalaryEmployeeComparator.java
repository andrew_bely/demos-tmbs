package org.taudev.demos.sorting.compare;

import java.util.Comparator;

import org.taudev.demos.sorting.model.Employee;

public class SalaryEmployeeComparator implements Comparator<Employee> {

    @Override
    public int compare(Employee o1, Employee o2) {
        return o1.getSalary().compareTo(o2.getSalary());
    }

}

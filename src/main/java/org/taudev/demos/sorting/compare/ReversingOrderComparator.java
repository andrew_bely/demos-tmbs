package org.taudev.demos.sorting.compare;

import java.util.Comparator;

public class ReversingOrderComparator<T> implements Comparator<T> {

    private Comparator<T> comparator;

    public ReversingOrderComparator(Comparator<T> comparator) {
        super();
        this.comparator = comparator;
    }

    @Override
    public int compare(T o1, T o2) {
        return -comparator.compare(o1, o2);
    }

}

package org.taudev.demos.sorting.compare;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ChainedComparator<T> implements Comparator<T> {

    private List<Comparator<T>> comparators;

    public ChainedComparator(List<Comparator<T>> cmp) {
        comparators = new ArrayList<Comparator<T>>(cmp.size());
        comparators.addAll(cmp);
    }

    @Override
    public int compare(T o1, T o2) {
        for (Comparator<T> cmp : comparators) {
            int result = cmp.compare(o1, o2);
            if (result != 0) {
                return result;
            }
        }
        return 0;
    }

}

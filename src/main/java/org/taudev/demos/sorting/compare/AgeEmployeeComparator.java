package org.taudev.demos.sorting.compare;

import java.util.Comparator;

import org.taudev.demos.sorting.model.Employee;

public class AgeEmployeeComparator implements Comparator<Employee> {

    @Override
    public int compare(Employee o1, Employee o2) {
        return -(o1.getBirthdate().compareTo(o2.getBirthdate()));
    }

}

package org.taudev.demos.sorting.compare;

public class ComparisonParam {

	private String name;
	private boolean reverse;
	
	public ComparisonParam(String name, boolean reverse) {
		super();
		this.name = name;
		this.reverse = reverse;
	}
	public ComparisonParam(String name) {
		super();
		this.name = name;
		this.reverse = false;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean isReverse() {
		return reverse;
	}
	
}

package org.taudev.demos.sorting.compare;

import java.util.Comparator;

import org.taudev.demos.sorting.model.Employee;

public class NameEmployeeComparator implements Comparator<Employee> {

    @Override
    public int compare(Employee o1, Employee o2) {
        int result = o1.getLastName().compareTo(o2.getLastName());
        if (result == 0) {
        	// firstName is nullable field, so consider null as minimal value
        	if (o1.getFisrtName() == null) {
        		result = (o2.getFisrtName() == null) ? 0 : -1;
        	}
        	else if (o2.getFisrtName() == null) {
        		result = 1;
        	}
        	else {
        		result = o1.getFisrtName().compareTo(o2.getFisrtName());
        	}
        }
        return result;
    }

}

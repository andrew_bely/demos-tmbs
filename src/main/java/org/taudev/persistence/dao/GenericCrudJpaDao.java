package org.taudev.persistence.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.metamodel.SingularAttribute;

public class GenericCrudJpaDao<T, PK extends Serializable> implements GenericDao<T, PK> {

    @PersistenceContext
    protected EntityManager em;

    protected Class<T> entityClass;

    protected GenericCrudJpaDao() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
    }

    @Override
    public T get(PK pk) {
        return em.find(entityClass, pk);
    }

    @Override
    public void save(T t) {
        try {
            em.merge(t);
        } catch (IllegalArgumentException e) {
            em.persist(t);
        }
    }

    @Override
    public void persist(T t) {
        em.persist(t);
    }

    @Override
    public void delete(T t) {
        em.remove(t);
    }

    protected <AT> CriteriaQuery<T> buildSingleParameterCriteriaQuery(SingularAttribute<T, AT> attr, AT value) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityClass);
        Expression<AT> attrExpr = criteriaQuery.from(entityClass).get(attr);
        criteriaQuery.where(criteriaBuilder.equal(attrExpr, value));
        return criteriaQuery;
    }

    @Override
    public <AT> T findOneByAttribute(SingularAttribute<T, AT> attr, AT value) {
        CriteriaQuery<T> criteriaQuery = buildSingleParameterCriteriaQuery(attr, value);
        return em.createQuery(criteriaQuery).getSingleResult();
    }

    @Override
    public <AT> List<T> findByAttribute(SingularAttribute<T, AT> attr, AT value) {
        CriteriaQuery<T> criteriaQuery = buildSingleParameterCriteriaQuery(attr, value);
        return em.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public List<T> loadAll() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityClass);
        criteriaQuery.from(entityClass);
        return em.createQuery(criteriaQuery).getResultList();
    }

}

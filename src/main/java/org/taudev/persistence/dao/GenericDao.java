package org.taudev.persistence.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.metamodel.SingularAttribute;

public interface GenericDao<T, PK extends Serializable> {

    T get(PK pk);

    <AT> T findOneByAttribute(SingularAttribute<T, AT> attr, AT value);

    <AT> List<T> findByAttribute(SingularAttribute<T, AT> attr, AT value);

    void save(T t);

    void persist(T t);

    void delete(T t);

    List<T> loadAll();
}
